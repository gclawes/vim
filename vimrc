" Install vim-plug if not found
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
endif

"set timeoutlen=500 " Set timeout length to 500 ms
"let mapleader=" "
"nnoremap <silent> <leader> :WhichKey '<Space>'<CR>
nnoremap <silent> <leader> :WhichKey '\'<CR>

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

function! Cond(cond, ...)
  let opts = get(a:000, 0, {})
  return a:cond ? opts : extend(opts, { 'on': [], 'for': [] })
endfunction

" coverd by jceb/vim-orgmode
let g:polyglot_disabled = ['org']

call plug#begin()

Plug 'airblade/vim-gitgutter', Cond(!exists('g:vscode'))
Plug 'editorconfig/editorconfig-vim', Cond(!exists('g:vscode'))
Plug 'hdiniz/vim-gradle'
Plug 'jceb/vim-orgmode'
Plug 'lervag/vimtex', Cond(!exists('g:vscode'))
Plug 'LunarWatcher/auto-pairs', Cond(!exists('g:vscode'))
"Plug 'majutsushi/tagbar', Cond(!exists('g:vscode'))
Plug 'mustache/vim-mustache-handlebars', Cond(!exists('g:vscode'))
"Plug 'neoclide/coc.nvim', Cond(!exists('g:vscode'), {'branch': 'release'})
Plug 'preservim/nerdtree'
Plug 'preservim/nerdcommenter'
Plug 'qpkorr/vim-renamer', Cond(!exists('g:vscode'))
Plug 'sheerun/vim-polyglot', Cond(!exists('g:vscode'))
"Plug 'tpope/vim-fugitive', Cond(!exists('g:vscode'))
Plug 'tpope/vim-git'
Plug 'tpope/vim-sensible'
"Plug 'tpope/vim-speeddating'
Plug 'tsandall/vim-rego'
Plug 'twerth/ir_black'
Plug 'vim-scripts/VisIncr'
Plug 'vimoutliner/vimoutliner'
Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] }

call plug#end()

let g:vimtex_enabled = 0

if executable('terraform')
  let g:terraform_fmt_on_save=1
  let g:terraform_align=1
endif

" specific neovim settings
if has('nvim')
  set guicursor=i:block
endif

" {{{ General settings
" The following are some sensible defaults for Vim for most users.
" We attempt to change as little as possible from Vim's defaults,
" deviating only where it makes sense
set nocompatible        " Use Vim defaults (much better!)
set tabstop=4           " numbers of spaces of tab character
set softtabstop=4       " soft tab stop
set shiftwidth=4        " shiftwidth
"set expandtab          " expand tabs to spaces
set scrolloff=3         " keep 3 lines when scrolling
set ignorecase          " ignore case when searching
set number				" show line numbers
set sm                  " show matching braces, somewhat annoying...
set nofoldenable

" Indent settings
set smartindent        " smart indent
"set cindent            " cindent

" When doing tab completion, give the following files lower priority. You may
" wish to set 'wildignore' to completely ignore files, and 'wildmenu' to enable
" enhanced tab completion. These can be done in the user vimrc file.
set suffixes+=.info,.aux,.log,.dvi,.bbl,.out,.o,.lo

" When displaying line numbers, don't use an annoyingly wide number column. This
" doesn't enable line numbers -- :set number will do that. The value given is a
" minimum width to use for the number column, not a fixed size.
if v:version >= 700
  set numberwidth=3
endif
" }}}

" {{{ Syntax highlighting settings
" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
  set background=dark
  colorscheme ir_black
endif

" }}}

" NERDTree
map <C-n> :NERDTreeToggle<CR>

" {{{ Autocommands
if has("autocmd")

  " taken from old ~/.vimrc
  " Restore cursor position
  au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif

  " automatically reseize windows when vim is resized
  au VimResized * wincmd =

  " Filetypes (au = autocmd)
  au FileType helpfile set nonumber      " no line numbers when viewing help
  au FileType helpfile nnoremap <buffer><cr> <c-]>   " Enter selects subject
  au FileType helpfile nnoremap <buffer><bs> <c-T>   " Backspace to go back

  " When using mutt, text width=72
  au FileType mail,tex set textwidth=72
  au FileType cpp,c,cl,java,sh,pl set autoindent
  au FileType cpp,c,cl,java,sh,pl set smartindent
  au FileType cpp,c,cl,java,sh,pl set cindent
  au FileType cpp,c,cl,java,sh,pl set noexpandtab
  au FileType cpp,c,cl,java,sh,pl set tabstop=2
  au FileType cpp,c,cl,java,sh,pl set shiftwidth=2
  au BufRead mutt*[0-9] set tw=72
 
  au BufNewFile,BufRead  *.cl 			  set syntax=c
  au BufNewFile,BufRead  modprobe.conf    set syntax=modconf
  
  autocmd BufNewFile,BufReadPost *.md,*.markdown set filetype=markdown

  au BufNewFile,BufRead  *.time	set expandtab

  au FileType yaml,sh,json set tabstop=2 shiftwidth=2 expandtab

endif " has("autocmd")
" }}}

" vim: set fenc=utf-8 tw=80 sw=2 sts=2 et foldmethod=marker :
